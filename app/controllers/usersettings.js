var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    UsersettingsModel = mongoose.model('usersettings');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/adminSetting', function(req, res, next) {
    var usersettingsModel = new UsersettingsModel(req.body);
    usersettingsModel.save(function(err, result) {
        if (err){
            console.log('adminSetting failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/allAdminSetting', function(req, res, next) {
UsersettingsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})

router.get('/adminBymongoId/:adminMongoId',function(req,res,next){
console.log('adminMongoId', req.params.adminMongoId);
UsersettingsModel.findOne({"_id":req.params.adminMongoId},function(err,result){
                    if(err)
                        {
                         console.log(err);
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

});

router.post('/editAdminBymongoId', function(req, res, next) {
        console.log("******", req.body)
        console.log(req.body._id);
            UsersettingsModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
            function(err,result)
                {
                    if(err){
                        console.log(err.stack)
                    }else{
                        res.send(result)
                    }

                });

})

router.delete('/adminBymongoId/:adminMongoId',function(req, res, next){
console.log('adminMongoId', req.params.adminMongoId);
UsersettingsModel.remove({"_id":req.params.adminMongoId},function(err,result)
{
if(err)
{
 console.log(err);
}
else
{
 res.send(result)
}

});
});



