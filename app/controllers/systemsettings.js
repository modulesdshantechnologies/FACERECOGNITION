var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    SystemsettingsModel = mongoose.model('systemsettings');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/systemSetting', function(req, res, next) {
    var systemsettingsModel = new SystemsettingsModel(req.body);
    systemsettingsModel.save(function(err, result) {
        if (err){
            console.log('adminSetting failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/allSystemSetting', function(req, res, next) {
SystemsettingsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})

router.get('/systemBymongoId/:systemMongoId',function(req,res,next){
console.log('systemMongoId', req.params.adminMongoId);
SystemsettingsModel.findOne({"_id":req.params.systemMongoId},function(err,result){
                    if(err)
                        {
                         console.log(err);
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

});

router.post('/editSystemBymongoId', function(req, res, next) {
        console.log("******", req.body)
        console.log(req.body._id);
            SystemsettingsModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
            function(err,result)
                {
                    if(err){
                        console.log(err.stack)
                    }else{
                        res.send(result)
                    }

                });

})

router.delete('/systemBymongoId/:systemMongoId',function(req, res, next){
console.log('systemMongoId', req.params.systemMongoId);
SystemsettingsModel.remove({"_id":req.params.systemMongoId},function(err,result)
{
if(err)
{
 console.log(err);
}
else
{
 res.send(result)
}

});
});



