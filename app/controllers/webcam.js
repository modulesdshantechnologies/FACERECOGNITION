var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    WebcamModel = mongoose.model('webcam');

    module.exports = function (app){
        app.use('/', router);
    };

router.post('/webcamphoto', function(req, res, next) {
var webcamModel = new WebcamModel(req.body);
    webcamModel.save(function(err, result) {
        if (err){
            console.log('photo failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/allWebcamPhoto', function(req, res, next) {
    WebcamModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})