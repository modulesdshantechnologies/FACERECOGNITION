var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ElectionResultSchema = new mongoose.Schema(
{
      gender:String,
      facialExpression:String,
      fromDate:Date,
      toDate:Date

},
{collection:"reportsettings"});
mongoose.model('reportsettings',ElectionResultSchema);
