var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ElectionResultSchema = new mongoose.Schema(
{
       employeeId:String,
       name:String,
       address:String,
       city:String,
       mobileNumber:Number

},
{collection:"employeeDetails"});
mongoose.model('employeeDetails',ElectionResultSchema);