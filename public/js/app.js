var webapp = angular.module('webapp', ['ui.router','ng-webcam','angularUtils.directives.dirPagination','ui.bootstrap','ui.bootstrap.datetimepicker']);

   webapp.config(function($stateProvider, $urlRouterProvider) {

       $urlRouterProvider.otherwise('/register');

       $stateProvider

       // HOME STATES AND NESTED VIEWS ========================================
           .state('register', {
               url: '/register',
               templateUrl: 'templates/register.html',
               controller:'registerController'
           })
           .state('listpage', {
              url: '/listpage',
              templateUrl: 'templates/listpage.html',
           })

           .state('settings',{
             url: '/settings',
             templateUrl: 'templates/settings.html',
           })

           .state('systemsettings',{
            url: '/systemsettings',
            templateUrl: 'templates/systemsettings.html',
            controller:'systemSettingController'

           })

           .state('adminsettings',{
           url: '/adminsettings',
           templateUrl: 'templates/adminsettings.html',
           controller:'adminSettingController'
           })

           .state('dashboard', {
             url: '/dashboard',
             templateUrl: 'templates/dashboard.html',
           })

           .state('reports', {
            url: '/reports',
            templateUrl: 'templates/reports.html',
            controller:'reportSettingController'
           })

           .state('captureImage',{
            url: '/captureImage',
            templateUrl: 'templates/captureimage.html',
            controller:'webcamController'
           })

           .state('employee', {
              url: '/employee',
              templateUrl: 'templates/employee.html',
              controller:'employeeController'
           })

           .state('employeeedit', {
             url: '/employeeedit/:employeeId',
             templateUrl: 'templates/employeeedit.html',
             controller:'employeeEditController'
           })

           .state('employeedetails', {
              url: '/employeedetails',
              templateUrl: 'templates/employeedetails.html',
              controller:'employeedetailcontroller'
           })

           // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
           .state('login', {
               url: '/login',
               templateUrl: 'templates/login.html',
               controller:'loginController'

               // we'll get to this in a bit
           });

   });