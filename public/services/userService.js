webapp.factory('userServices',function($http){
   var postAllRegisterDetails = function(data)
        {
           return $http.post('/userRegistration', data);
        };

   var postAllLoginDetails = function(data)
        {
            console.log("testing:::::", data)
           return $http.post('/login', data);
        };
   var postEmployeedetails=function(data)
           {
              return $http.post('/employeeDetails',data);
           }
  var getAllEmployeeDetails = function()
       {
           return $http.get('/allEmployeeDetails');
       }
  var deleteEmployee=function(employeeId)
        {
           return $http.delete('/employeeBymongoId/'+employeeId);
        }
  var getSingleEmployeeDetail=function(employeeId)
             {
                return $http.get('/singleemployeeBymongoId/'+employeeId);
             }
  var updateEmployeeDetails=function(editdata)
           {
              return $http.post('/editEmployeeBymongoId',editdata);
           }
   var getAllAdminSettings=function()
               {
                  return $http.get('/allAdminSetting');
               }
       var postAdminSettings=function(data)
                {
                   return $http.post('/adminSetting',data);
                }
       var deleteAdmin=function(adminId)
             {
                return $http.delete('/adminBymongoId/'+adminId);
             }

      var getSingleAdmin =function(adminId)
                {
                   return $http.get('/adminBymongoId/'+adminId);
                }
      var updateAdmin=function(editdata)
                   {
                      return $http.post('/editAdminBymongoId',editdata);
                   }
      var postSystemSettings=function(data)
                   {
                      return $http.post('/systemSetting',data);
                   }
      var deleteSystem=function(systemId)
            {
               return $http.delete('/systemBymongoId/'+systemId);
            }
      var getAllSystemSettings=function()
                  {
                     return $http.get('/allSystemSetting');
                  }
      var getSingleSystem =function(systemId)
               {
                  return $http.get('/systemBymongoId/'+systemId);
               }
      var updateSystem=function(editdata)
                      {
                         return $http.post('/editSystemBymongoId',editdata);
                      }

      var postReportSettings=function(data)
                         {
                            return $http.post('/reportSetting',data);
                         }
        var deleteReport=function(routeId)
              {
                 return $http.delete('/reportBymongoId/'+routeId);
              }
        var getAllReportSettings=function()
                    {
                       return $http.get('/allReportSetting');
                    }
        var getSingleReport =function(routeId)
                 {
                    return $http.get('/reportBymongoId/'+routeId);
                 }
        var updateReport=function(editdata)
                        {
                           return $http.post('/editReportBymongoId',editdata);
                        }
   return   {
             postAllRegisterDetails: postAllRegisterDetails,
             postAllLoginDetails: postAllLoginDetails,
             postEmployeedetails: postEmployeedetails,
             getAllEmployeeDetails: getAllEmployeeDetails,
             deleteEmployee: deleteEmployee,
             getSingleEmployeeDetail: getSingleEmployeeDetail,
             updateEmployeeDetails: updateEmployeeDetails,
             getAllAdminSettings: getAllAdminSettings,
               postAdminSettings: postAdminSettings,
               deleteAdmin: deleteAdmin,
               getSingleAdmin: getSingleAdmin,
               updateAdmin:updateAdmin,
               postSystemSettings: postSystemSettings,
               deleteSystem: deleteSystem,
               getAllSystemSettings: getAllSystemSettings,
               getSingleSystem: getSingleSystem,
               updateSystem:updateSystem,
               postReportSettings:postReportSettings,
               deleteReport:deleteReport,
               getAllReportSettings: getAllReportSettings,
               getSingleReport:getSingleReport,
               updateReport:updateReport
            }

   });
